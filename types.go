package gocli

import (
	"strings"
)

type rawCommand struct {
	Content string
}

type pipeline struct {
	Commands []command
}

type command struct {
	Name      string
	Arguments []string
}

type Instruction interface {
	ToString() string
}

func (executable *command) ToString() string {
	formatted := strings.Join(append([]string{executable.Name}, executable.Arguments...), Whitespace)
	trimmed := normalize(formatted)
	return trimmed
}

/*
Constructors
*/

func newCommand(executable string, args ...string) *command {
	if args == nil {
		args = []string{}
	}
	return &command{
		Name:      executable,
		Arguments: args,
	}
}

func newPipeline(commands ...command) *pipeline {
	return &pipeline{
		Commands: commands,
	}
}

func newRawCommand(command string) *rawCommand {
	return &rawCommand{
		Content: command,
	}
}
