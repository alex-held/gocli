package gocli

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestToStdCmd(t *testing.T) {
	pipeline, err := ParsePipeline("cat ~/.bashrc | less")
	assert.NoError(t, err)
	expected := []command{
		*newCommand("cat", "~/.bashrc"),
		*newCommand("less"),
	}
	actual := pipeline.Commands
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)
}
