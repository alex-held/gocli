package gocli

import (
	"fmt"
)

type Action = func(ex Instruction) error

type Executable interface {
	Run(action Action) error
}

func (executable *command) Run(action Action) error {
	return action(executable)
}

var NoOpAction Action = func(ex Instruction) error { return nil }

var PrintAction Action = func(ex Instruction) error {
	switch executable := ex.(type) {
	case *command:
		text := executable.ToString()
		fmt.Println(text)
	}

	return nil
}
