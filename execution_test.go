package gocli

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrinter(t *testing.T) {
	expected := "cat ~/.bashrc"
	DumpExpected(expected)
	cmd := newCommand(expected)

	out := captureStdOut(func() {
		_ = cmd.Run(PrintAction)
	})

	DumpActual(out)
	assert.Contains(t, out, expected)
	if !t.Failed() {
		Success()
	}
}
