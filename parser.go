package gocli

import (
	"fmt"
	"strings"
)

type Token = string

const (
	Whitespace Token = " "
	Pipe       Token = "|"
	Empty      Token = ""
	NewLine    Token = "\n"
)

/*
Parse string
*/

func ParsePipeline(input string) (pipeline *pipeline, err error) {
	var commands []command
	for i, cmd := range strings.Split(input, Pipe) {
		command, err := ParseCommand(cmd)
		if err != nil {
			return nil, fmt.Errorf("Unable to parse pipeline due to invalid subcommand at pipeline position %v.\n--\n%v", i, err)
		}
		commands = append(commands, *command)
	}
	pipeline = newPipeline(commands...)
	return pipeline, nil
}

// ParseCommand parses strings or error if it contains forbidden characters
func ParseCommand(input string) (command *command, err error) {
	forbiddenTokens := []string{
		Pipe,
	}
	forbidden := strings.Join(forbiddenTokens, Empty)

	if strings.ContainsAny(input, forbidden) {
		return nil, fmt.Errorf("Unable to parse input '%s' into command. It contains forbidden characters.\nForbidden characters are: %#v", input, forbiddenTokens)
	}

	splitted := split(input)
	executable := splitted[0]
	args := splitted[1:]

	return newCommand(executable, args...), nil
}
