package gocli

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParsePipeline(t *testing.T) {
	var parseTests = []struct {
		input    string
		expected pipeline
	}{
		{input: "echo 'Hello World'", expected: *newPipeline(*newCommand("echo", "'Hello World'"))},
		{input: "echo 'Hello Pipeline' | less", expected: *newPipeline(*newCommand("echo", "'Hello Pipeline'"), *newCommand("less"))}, // nolint:lll
	}

	for _, tt := range parseTests {
		actual, _ := ParsePipeline(tt.input)
		printInfo(fmt.Sprintf("Parsing input `%s`", tt.input))

		assert.IsType(t, tt.expected, *actual)
		assert.Equal(t, tt.expected, *actual)
		Success()
	}

}

func TestParseCommand(t *testing.T) {
	var parseTests = []struct {
		input    string
		expected interface{}
	}{
		{input: "echo 'Hello World'", expected: newCommand("echo", "'Hello World'")},
		{input: "echo 'Hello Fail' | less"},
	}

	for _, tt := range parseTests {
		actual, err := ParseCommand(tt.input)
		printInfo(fmt.Sprintf("Parsing input `%s`", tt.input))

		if tt.expected == nil {
			printInfo("Expecting Parse to fail.")
			if err == nil {
				t.Errorf("Expected parsing of input `%s` to fail, but it didn't.\n", tt.input)
			}
			Success()
		} else {
			assert.IsType(t, tt.expected, actual)
			assert.Equal(t, tt.expected, actual)
			Success()
		}
	}
}
