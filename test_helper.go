package gocli

import (
	"fmt"
	"strings"

	ansi2 "github.com/alex-held/gocli/ansi"
)

const ansiEnabled = true

func printInfo(msg string) {
	dump("test", msg, ansi2.AnsiYellow)
}

func Success() {
	dump("Success", "", ansi2.AnsiGreen)
}

func DumpMessage(tag string, message string) {
	dump(tag, message, ansi2.AnsiMagenta)
}

func DumpExpected(expected interface{}) {
	dump("Expected", fmt.Sprintf("%+v", expected), ansi2.AnsiRed)
}

func DumpActual(actual interface{}) {
	dump("Actual", fmt.Sprintf("%v", actual), ansi2.AnsiCyan)
}

func dump(tag string, message string, color ansi2.AnsiColor) {
	tag = fmt.Sprintf("[%s]", strings.ToUpper(tag))
	if ansiEnabled {
		tag = "\033[1;" + color + tag + "\033[0m"
		message = ansi2.Color(message, color)
	}
	println(tag + "\t" + message)

}
