package ansi

import (
	"io"
)

func PrintGreen(msg string, writer io.Writer) (string, error) {
	return PrintColor(msg, AnsiGreen, writer)
}

func PrintCyan(msg string, writer io.Writer) (string, error) {
	return PrintColor(msg, AnsiCyan, writer)
}

func PrintYellow(msg string, writer io.Writer) (string, error) {
	return PrintColor(msg, AnsiYellow, writer)
}

func PrintRed(msg string, writer io.Writer) (string, error) {
	return PrintColor(msg, AnsiRed, writer)
}

func PrintColor(msg string, color AnsiColor, writer io.Writer) (string, error) {
	msg = Color(msg+"\n", color)
	_, err := writer.Write([]byte(msg))
	return msg, err
}
