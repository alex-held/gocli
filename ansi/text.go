package ansi

func Color(msg string, color AnsiColor) string {
	colored := AnsiStart + color + msg + AnsiReset
	return colored
}

func Red(msg string) string {
	return Color(msg, AnsiRed)
}
