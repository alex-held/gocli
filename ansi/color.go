package ansi

type AnsiColor = string

const (
	AnsiStart   AnsiColor = "\033["
	AnsiReset   AnsiColor = "\033[0m"
	AnsiBlack   AnsiColor = "90m"
	AnsiRed     AnsiColor = "91m"
	AnsiGreen   AnsiColor = "92m"
	AnsiYellow  AnsiColor = "93m"
	AnsiMagenta AnsiColor = "95m"
	AnsiCyan    AnsiColor = "96m"
	AnsiWhite   AnsiColor = "97m"
)
