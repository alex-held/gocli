package gocli

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSplit(t *testing.T) {
	expected := []string{
		"cat",
		"~/.bashrc",
		"'Hello World1'",
		"\"Hello World2\"",
		"o",
		"ther\"",
	}
	input := "     cat ~/.bashrc 'Hello World1' \"Hello World2\" o ther\""
	actual := split(input)
	DumpMessage("Input", input)
	DumpExpected(expected)
	DumpActual(actual)
	assert.Equal(t, expected, actual)
}
