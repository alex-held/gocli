<h1 align="center">Welcome to gocli 👋</h1>
<p>
  <a href=" https://gocli.alexheld.io" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://www.gnu.org/licenses/gpl-3.0.html" target="_blank">
    <img alt="License: GNU GPLv3" src="https://img.shields.io/badge/License-GNU GPLv3-yellow.svg" />
  </a>
  <a href="https://twitter.com/0\_alexheld" target="_blank">
    <img alt="Twitter: 0\_alexheld" src="https://img.shields.io/twitter/follow/0\_alexheld.svg?style=social" />
  </a>
</p>

>  tools for rapid go cli development 

### 🏠 [Homepage]( https://gocli.alexheld.io)

## Install

```sh
go get -u github.com/alex-held/gocli
```

## Run tests

```sh
go test ./... -v -cover
```

## Author

👤 **alexander held**

* Website: https://alexheld.io
* Twitter: [@0\_alexheld](https://twitter.com/0\_alexheld)
* Github: [@alex-held](https://github.com/alex-held)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/alex-held/gocli/issues). You can also take a look at the [contributing guide](https://github.com/alex-held/gocli/blob/master/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2020 [alexander held](https://github.com/alex-held).<br />
This project is [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_