package gocli

import (
	"bytes"
	"io"
	"os"
	"strings"
)

func normalize(input string) string {
	normalized := strings.Trim(input, Whitespace)
	return normalized
}

func captureStdOut(closure func()) string {
	old := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	closure()

	outC := make(chan string)
	go func() {
		var buf bytes.Buffer
		_, _ = io.Copy(&buf, r)
		outC <- buf.String()
	}()

	_ = w.Close()
	os.Stdout = old
	out := <-outC
	return out
}

func split(input string) (commands []string) {
	isLiteral := func(input string) bool {
		c := string(input[0])
		return c == "\"" || c == "'"
	}
	nextLiteral := func(input string) (field string, remainder string) {
		token := string(input[0])
		start := strings.Index(input, token) + 1
		end := strings.Index(input[start:], token) + 1
		remainder = input[end+1:]
		field = input[start-1 : end+1]
		return field, remainder
	}

	parseNext := func(input string) (next string, remainder string) {
		input = strings.TrimSpace(input)
		if isLiteral(input) {
			next, remainder = nextLiteral(input)
		} else {
			p := strings.Split(input, " ")
			next = p[0]
			remainder = strings.Join(p[1:], " ")
		}

		return next, remainder
	}

	remaining := input
	var next string

	for len(remaining) > 0 {
		next, remaining = parseNext(remaining)
		commands = append(commands, next)
	}
	return commands
}
